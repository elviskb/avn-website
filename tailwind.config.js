/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    './node_modules/@ipaat/vue3-tailwind3-cookie-comply/dist/vue3-tailwind3-cookie-comply.umd.js',
  ],
  theme: {
    extend: {
      fontFamily: {
        display: ['Nunito'],
        sans: ['Nunito'],
        serif: ['Nunito'],
        body: ['Nunito'],
      }
    },
  },
  plugins: [],
}
