import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import OfferView from '../views/OfferView.vue'
import WhyAVNView from '../views/WhyAVNView.vue'
import SignUpView from '../views/SignUpView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about-us',
    name: 'about',
    component: AboutView
  },
  {
    path: '/our-offering',
    name: 'what-we-offer',
    component: OfferView
  },
  {
    path: '/why-avn',
    name: 'why-avn',
    component: WhyAVNView
  },
  {
    path: '/sign-up',
    name: 'sign-up',
    component: SignUpView
  },
]

const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition;
  }
  if (to.hash) {
    return { el: to.hash };
  }
  return { x: 0, y: 0 };
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior
})

export default router
