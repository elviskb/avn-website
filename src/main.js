import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import VueCookieComply from '@ipaat/vue3-tailwind3-cookie-comply'

import './assets/main.css'

const app = createApp(App)
app.component('VueCookieComply', VueCookieComply)

app.use(router)

app.mount('#app')
